'use strict'

const chromedriver = require('chromedriver');
const { remote } = require('webdriverio');
const expect = require('chai').expect;

let browser;

beforeSuite(function () {
    const args = [
        '--port=4444'
    ]

    chromedriver.start(args);
});

beforeSpec(async function () {
    browser = await remote({
        logLevel: 'trace',
        capabilities: {
            browserName: 'chrome'
        }
    });
});

afterSpec(async function () {
    await browser.deleteSession();
});

afterSuite(function () {
    chromedriver.stop();
});


step("Navigate to <url>", async function(url) {
    await browser.url(url);
});

step("Search for <searchText>", async function(searchText) {
    const inputElem = await browser.$('#search_form_input_homepage');
    await inputElem.setValue(searchText);
    const submitBtn = await browser.$('#search_button_homepage');
    await submitBtn.click();
});

step("Expect the page title to contain <arg0>", async function(arg0) {
    expect(await browser.getTitle()).to.match(/WebDriverIO/);
});
